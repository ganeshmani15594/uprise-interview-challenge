import styled from "styled-components";

export const Container = styled.div`
  margin: auto;
  display: flex;
  flex-direction: column;
  width: 920px;
  margin-top: 120px;
`;

export const LoginButton = styled.a`
  background-color: transparent;
  border-radius: 2em;
  border: 0.2em solid #1ecd97;
  color: #1ecd97;
  cursor: pointer;
  font-size: 3vmin;
  padding: 0.7em 1.5em;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  transition: all 0.25s ease;

  :hover {
    background: #1ecd97;
    color: #333;
  }
`;
