import React, { useState, useEffect } from "react";
import PlaylistItem from "./PlaylistItem";
import LeftArrow from "../left_arrow.svg";
import RightArrow from "../right_arrow.svg";
import {
  Container,
  PlayListContainer,
  PlayLists,
  PlayListArrowContainer,
  PlayListRightArrowContainer,
  ArrowImage,
} from "./Playlist.styles";

const PlayList = ({ playlists }) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const previousList = () => {
    if (activeIndex < 1) {
      setActiveIndex(playlists.length - 1);
    } else {
      setActiveIndex(activeIndex - 1);
    }
  };

  const nextList = () => {
    if (activeIndex === playlists.length - 1) {
      setActiveIndex(0);
    } else {
      setActiveIndex(activeIndex + 1);
    }
  };
  return (
    <Container aria-labelledby="playlist-tab" role="tabpanel">
      <PlayListContainer>
        <ArrowImage onClick={previousList}>
          <img src={LeftArrow} />
        </ArrowImage>

        <PlayLists>
          {playlists.map((playlist, index) => (
            <PlaylistItem
              key={index}
              index={index}
              activeIndex={activeIndex}
              playlist={playlist}
            />
          ))}
        </PlayLists>

        <ArrowImage onClick={nextList}>
          <img src={RightArrow} />
        </ArrowImage>
      </PlayListContainer>
    </Container>
  );
};

export default PlayList;
