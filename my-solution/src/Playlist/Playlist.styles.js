import styled from "styled-components";

export const Container = styled.div`
  width: 970px;
  padding: 40px 0;
  border-radius: 10px;
  box-shadow: 1px 1px 1px 1px #dbdde3;
  background-color: #fff;
  margin-top: 36px;
`;

export const PlayListContainer = styled.div`
  max-width: 600px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PlayLists = styled.ul`
  max-width: 900px;
  list-style-type: none;
  text-align: center;
  width: 320px;
  margin: 0 40px;
  padding: 0;
`;

export const PlayListArrowContainer = styled.div`
  border: 1px solid #7d60ff;
  border-radius: 25px;
  width: 50px;
  height: 50px;
  margin: auto 40px auto 0;
  cursor: pointer;
`;

export const PlayListRightArrowContainer = styled.div`
  /* border: 1px solid #7d60ff;
  border-radius: 25px;
  width: 50px;
  height: 50px;
  margin: auto 0 auto 40px;
  cursor: pointer; */
`;

export const ArrowImage = styled.figure`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #7d60ff;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
  margin: 0;
`;
