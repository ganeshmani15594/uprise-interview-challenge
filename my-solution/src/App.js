import React, { useState, useEffect, Fragment } from "react";
import "./App.css";
import { Container, LoginButton } from "./App.styles";
import { SpotifyGraphQLClient } from "spotify-graphql";
import Navbar from "./Navbar/Navbar";
import Overview from "./Overview/Overview";
import config from "./config";
import hash from "./hash";
import PlayList from "./Playlist/Playlist";
import Featured from "./Featured/Featured";

const USER_QUERY = `{
  user(id :"chilledcow"){
    id
    images{
      url
    }
    display_name
    playlists{
      id
      description
      href
      name
      tracks{
        track{
          artists{
            id
            name
          }
        }
      }
      owner{
        display_name
        id
      }
      uri
      images{
        url
      }
    }
  }
}`;

const renderSectionCard = (currentTab, user) => {
  switch (currentTab) {
    case "OVERVIEW":
      return <Overview user={user} />;

    case "PLAYLIST":
      return <PlayList playlists={user.playlists} />;
    case "FEATURED":
      return <Featured playlists={user.playlists} />;
    default:
      return null;
  }
};

function App() {
  const [currentTab, setCurrentTab] = useState("");
  const [configState, setConfigState] = useState({});
  const [user, setUser] = useState({});

  useEffect(() => {
    let _token = hash.access_token;

    if (_token) {
      let config_data = config;
      config_data.accessToken = _token;
      setConfigState(config_data);

      SpotifyGraphQLClient(config_data)
        .query(USER_QUERY)
        .then((res) => {
          if (res.errors) {
            console.log(`Error => ${JSON.stringify(res.errors)}`);
          } else {
            console.log("rsss", res.data.user);
            setUser(res.data.user);
          }
        });
    }

    setCurrentTab("OVERVIEW");
  }, []);

  return (
    <Container>
      {!configState.accessToken ? (
        <LoginButton
          href={`${config.authEndpoint}?client_id=${
            config.clientId
          }&redirect_uri=${config.redirectUri}&scope=${config.SCOPES.join(
            "%20"
          )}&response_type=token&show_dialog=true`}
        >
          Login to Spotify
        </LoginButton>
      ) : (
        <Fragment>
          <Navbar
            currentTab={currentTab}
            onTabChange={(value) => setCurrentTab(value)}
          />
          {renderSectionCard(currentTab, user)}
        </Fragment>
      )}
    </Container>
  );
}

export default App;
