import React, { useState, useEffect } from "react";
import { getFeaturedArtists } from "../util";
import useKeyPress from "../custom-hooks/useKeyPress";
import {
  Container,
  ArtistsList,
  ArtistListItem,
  ArtistItemName,
} from "./Featured.styles";
const FeaturedArtists = ({ playlists }) => {
  const [artists, setArtists] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);

  const downPress = useKeyPress("ArrowDown");
  const upPress = useKeyPress("ArrowUp");

  useEffect(() => {
    setArtists(getFeaturedArtists(playlists));
  }, []);

  useEffect(() => {
    if (artists.length && downPress) {
      setActiveIndex((prevState) =>
        prevState < artists.length - 1 ? prevState + 1 : prevState
      );
    }
  }, [downPress]);
  useEffect(() => {
    if (artists.length && upPress) {
      setActiveIndex((prevState) =>
        prevState > 0 ? prevState - 1 : prevState
      );
    }
  }, [upPress]);

  return (
    <Container aria-labelledby="featured-tab" role="tabpanel">
      <ArtistsList>
        {artists.map((artist, index) => (
          <ArtistListItem
            key={index}
            style={{
              backgroundColor: activeIndex === index ? "#7d60ff" : "#fff",
              color: activeIndex === index ? "#fff" : "#6d6c79",
            }}
          >
            <ArtistItemName>{artist.name}</ArtistItemName>
          </ArtistListItem>
        ))}
      </ArtistsList>
    </Container>
  );
};

export default FeaturedArtists;
