import styled from "styled-components";

export const Container = styled.div`
  max-width: 200px;
  margin-left: 229px;
  margin-top: 2px;
  box-shadow: 1px #9898bb;
`;

export const ArtistsList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
  border-radius: 5px;
`;

export const ArtistListItem = styled.li`
  color: #6d6c79;
  height: 40px;
  width: 100%;
  cursor: pointer;
  background-color: #fff;
  font-size: 15px;
  :hover {
    background-color: #7d60ff;
    color: white;
  }

  &:first-child {
    border-radius: 5px 5px 0 0;
  }

  &:last-child {
    border-radius: 0 0 5px 5px;
  }
`;

export const ArtistItemName = styled.p`
  height: 16px;
  margin: 0;
  padding: 12px;
`;
