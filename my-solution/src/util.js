export const getFeaturedArtists = (playlists) => {
  let tracks = [];
  let artists = [];
  playlists.forEach((item) => {
    tracks = tracks.concat(item.tracks);
  });

  tracks.forEach((track) => {
    artists = artists.concat(track.track.artists);
  });

  let random_artists = artists.sort(() => 0.5 - Math.random()).slice(0, 10);

  return random_artists;
};
