import React from "react";
import { Container, NavList, NavContainer, NavLink } from "./Navbar.styles";
const Navbar = ({ currentTab, onTabChange }) => {
  return (
    <Container>
      <NavList role="tablist" aria-orientation="horizontal">
        <NavContainer
          tabIndex={1}
          onClick={() => onTabChange("OVERVIEW")}
          onKeyDown={(e) => {
            console.log("overview e", e.keyCode);
            if (e.keyCode === 13) {
              onTabChange("OVERVIEW");
            }
          }}
          role="presenation"
        >
          <NavLink
            role="tab"
            aria-selected={currentTab === "OVERVIEW" ? "true" : "false"}
            id="overview-tab"
            aria-controls="overview-content-panel"
            style={{
              color: currentTab === "OVERVIEW" ? "#7D60FF" : "",
            }}
          >
            Overview
          </NavLink>
        </NavContainer>
        <NavContainer
          tabIndex={2}
          role="presenation"
          onKeyDown={(e) => {
            if (e.keyCode === 13) {
              onTabChange("PLAYLIST");
            }
          }}
          onClick={() => onTabChange("PLAYLIST")}
        >
          <NavLink
            role="tab"
            aria-selected={currentTab === "PLAYLIST" ? "true" : "false"}
            id="playlist-tab"
            aria-controls="playlist-content-panel"
            style={{
              color: currentTab === "PLAYLIST" ? "#7D60FF" : "",
            }}
          >
            Playlist
          </NavLink>
        </NavContainer>
        <NavContainer
          tabIndex={3}
          role="presenation"
          onKeyDown={(e) => {
            if (e.keyCode === 13) {
              onTabChange("FEATURED");
            }
          }}
          onClick={() => onTabChange("FEATURED")}
        >
          <NavLink
            role="tab"
            aria-selected={currentTab === "FEATURED" ? "true" : "false"}
            id="featured-tab"
            aria-controls="featured-content-panel"
            style={{
              color: currentTab === "FEATURED" ? "#7D60FF" : "",
            }}
          >
            Featured
          </NavLink>
        </NavContainer>
      </NavList>
    </Container>
  );
};

export default Navbar;
