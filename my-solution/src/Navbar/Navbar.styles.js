import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  background-color: #fff;
  display: flex;
  padding: 25px;
  border-radius: 10px;
  box-shadow: 1px 1px 1px 1px #dbdde3;
`;

export const NavList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

export const NavContainer = styled.li`
  height: 20px;
  margin: 0 0 0 40px;
  float: left;
  cursor: pointer;
  text-decoration: none;
  /* :hover {
    background-color: #edeafa;
  } */

  :active {
    background-color: #7d60ff;
  }
`;

export const NavLink = styled.a``;
