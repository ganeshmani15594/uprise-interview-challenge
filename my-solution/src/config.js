export default {
  clientId: "c83bb6bf62eb42229f300aa78243cb1a",
  clientSecret: "98b2c12e46134f4b9fdd0847572727e6",
  redirectUri: "http://localhost:3001/",
  accessToken: "",
  authEndpoint: "https://accounts.spotify.com/authorize",
  SCOPES: [
    "user-read-recently-played",
    "user-library-read",
    "user-library-modify",
    "playlist-read-private",
    "user-top-read",
    "playlist-modify-public",
    "playlist-modify-private",
    "user-follow-read",
  ],
};
