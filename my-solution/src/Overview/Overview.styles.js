import styled from "styled-components";

export const Container = styled.div`
  width: 970px;
  border-radius: 10px;
  box-shadow: 1px 1px 1px 1px #dbdde3;
  background-color: #fff;
  margin-top: 36px;
`;

export const OverviewContainer = styled.div`
  display: flex;
  padding: 40px;
`;

export const UserDetailsContainer = styled.div`
  display: flex;
  margin-left: 40px;
  flex-direction: column;
`;

export const Image = styled.img`
  border-radius: 10px;
  width: 320px;
  height: 240px;
`;

export const UserTitle = styled.h2`
  margin: 0;
  color: #2f2d40;
`;

export const FollowerCount = styled.p``;

export const FollowButton = styled.button`
  background-color: #7d60ff;
  margin-top: auto;
  width: 120px;
  height: 42px;
  color: #fff;
  border-radius: 10px;
`;
