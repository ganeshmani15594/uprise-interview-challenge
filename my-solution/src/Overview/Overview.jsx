import React, { useState, useEffect } from "react";
import {
  Container,
  OverviewContainer,
  UserDetailsContainer,
  Image,
  UserTitle,
  FollowerCount,
  FollowButton,
} from "./Overview.styles";

const Overview = ({ user }) => {
  return (
    <Container aria-labelledby="overview-tab" role="tabpanel">
      <OverviewContainer>
        <Image src={user && user.images ? user.images[0].url : ""} />
        <UserDetailsContainer>
          <UserTitle>{user.display_name}</UserTitle>
          <FollowerCount>Followers(123,123)</FollowerCount>

          <FollowButton>Follow</FollowButton>
        </UserDetailsContainer>
      </OverviewContainer>
    </Container>
  );
};

export default Overview;
